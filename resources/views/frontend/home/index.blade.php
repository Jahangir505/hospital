<x-frontend.layouts.master>
    <x-slot:title>Front Page</x-slot:title>
    <x-slot:view>Home</x-slot:view>
<div class="banner">
    <div class="slider">
        <img src="img/slider/slider1.jpg" alt="" id="sliderImg">
    </div>
    <div class="overlay">
        <div class="content">
            <h2>WE PROVIDE BEST HEALTHCARE</h2>
            <p>Explicabo esse amet tempora quibusdam laudantium, laborum eaque magnam fugiat hic? Esse dicta aliquid
                error repudiandae earum suscipit fugiat molestias, veniam, vel architecto veritatis delectus
                repellat
                modi impedit sequi.</p>
            <button>Read More</button>
        </div>
    </div>
</div>
</x-frontend.layouts.master>