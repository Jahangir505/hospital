<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/main.css">
</head>

<body>
    <div class="login-page">
                    
        <div class="form">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <form class="login-form" method="POST" action="{{ route('login') }}">
                @csrf
                <input type="email" placeholder="email" name="email" />
                <input type="password" placeholder="password" name="password" />
                <div style="width: 60% ; display:flex;">
                    <input style="width:10% ;" type="checkbox" placeholder="Remember me" /> <span
                        style="color:#ffffff ; padding-left: 5px;">Remember me</span>
                </div>
                <button>login</button>
                <p class="message">Not registered? <a href="register.html">Create an account</a></p>
                <a href="{{ route('home') }}" style="background-color: black; display:flex; align-items:center; justify-content:center; margin: 0 auto; margin-top: 20px; padding: 10px 30px; color: #ffffff; text-decoration: none;">Go to home</a> 
            </form>
        </div>
    </div>
</body>

</html>