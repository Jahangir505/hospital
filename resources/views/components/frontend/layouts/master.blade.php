<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title ?? "Hospital"}}</title>
    <link rel="shortcut icon" href="img/favicon.png" type="">
    <link rel="stylesheet" href="{{ asset('/frontend') }}/css/main.css">

</head>

<body onload="slider()">
    <x-frontend.layouts.partials.header/>

    {{ $slot ?? "Content not found!" }}

    <script>
        var sliderImg = document.getElementById('sliderImg');
        var images = new Array(
            "{{ asset('/frontend') }}/img/slider/slider1.jpg",
            "{{ asset('/frontend') }}/img/slider/slider2.jpg",
            "{{ asset('/frontend') }}/img/slider/slider3.jpg"
        );
        var len = images.length;
        var i = 0;
        function slider() {
            if (i > len - 1) {
                i = 0;
            }

            sliderImg.src = images[i];
            i++;
            setTimeout('slider()', 3000);
        }
    </script>

</body>

</html>