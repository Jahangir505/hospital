<x-admin.layouts.master>
    <x-slot:title>Users </x-slot:title>
    <x-slot:pageTitle>Create User </x-slot:pageTitle>
    <div class="card p-4">
        <div class="card-header">
            
           
            <a href="{{route('users.index')}}" class="btn btn-info">User List</a>
        </div>






        <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div class="row">


                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="name" placeholder="Enter your full name" label="Full Name:" labelFor="name"  id="name" type="text" value="{{ old('name') }}" /> 
                        <x-admin.utilities.form.error name="name" />
                    </div>
                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="username" placeholder="Enter your username" label=" Username:" labelFor="username"  id="username" type="text" value="{{ old('username') }}" /> 
                        <x-admin.utilities.form.error name="username" />
                    </div>

                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="email" placeholder="Enter your Email Address" label="Email:" labelFor="email"  id="email" type="email" value="{{ old('email') }}" /> 
                        <x-admin.utilities.form.error name="email" />
                    </div>

                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="phone" placeholder="Enter your Phone Number" label="Phone Number" labelFor="phone"  id="phone" type="number" value="{{ old('phone') }}" /> 
                        <x-admin.utilities.form.error name="phone" />
                    </div>

                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="password" placeholder="Enter your password" label="Password" labelFor="password"  id="password" type="password" value="{{ old('password') }}" /> 
                        <x-admin.utilities.form.error name="password" />
                    </div>

                    <div class="col-md-6">
                        <x-admin.utilities.form.input name="password_confirmation" placeholder="Confirm Password" label="Confirm Password" labelFor="password_confirmation"  id="password_confirmation" type="password" value="{{ old('password_confirmation') }}" /> 
                        <x-admin.utilities.form.error name="password_confirmation" />
                    </div>
                    
                    <div class="col-md-6">
                        <label for="" class="form-label mt-3">Select Role</label>
                       <select class="form-control" name="role_id" id="">
                            <option value="">Select Role</option>
                            @foreach ($user_roles as $item)
                                <option value="{{ $item->id }}">{{$item->name}}</option>
                            @endforeach
                       </select>
                    </div>
                    

               
                    
                </div>
                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </div>
           
           
            
        </form>
    </div>

</x-admin.layouts.master>