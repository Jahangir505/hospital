<x-admin.layouts.master>
    <x-slot:title>Users </x-slot:title>
    <x-slot:pageTitle>User List </x-slot:pageTitle>
    <div class="card mb-4">
        <div class="card-header" style="display: flex; justify-content: space-between; align-items:center;">
            <span><i class="fas fa-table me-1"></i>
                DataTable Example</span>
            <a href="{{ route('users.create') }}" class="btn btn-primary">Add New</a>
            
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($users as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user_name}}</td>
                            <td>{{$item->phone}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->user_role->name}}</td>
                            <td>
                                <a href="" class="btn btn-sm btn-primary">Edit</a>
                                <a href="" class="btn btn-sm btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</x-admin.layouts.master>